function create_table_with_random(){
    while(true){
        var rows = parseInt(prompt("Enter Rows [2; 10]: "))
        if(rows>=2 && rows<=10){
            break
        }
    }

    while(true){
        var cols = parseInt(prompt("Enter Cols [2; 10]: "))
        if(cols>=2 && cols<=10){
            break
        }
    }
    
    let result = document.querySelector("#result")
    let tb = document.createElement("table")
    for(let i=0; i<rows; i++){
        let tr = document.createElement("tr")
            for(let j=0; j<cols; j++){
                let td = document.createElement("td")
                if(Math.random() < 0.5){
                    let r = Math.floor(Math.random()*51)+50
                    td.innerText = r
                }
                tr.appendChild(td)
            }
        tb.appendChild(tr)
    }
    tb.classList.add("tb")
    result.appendChild(tb);
}

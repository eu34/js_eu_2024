function practice2_task1(a){
    document.write(Math.ceil(a))
}

// practice2_task1(63.6)
// document.write("<hr>")
// practice2_task1(23.2)

function practice2_task4(n, m){
    if(m=='c'){
        document.write(`<h2>${Math.ceil(n)}</h2>`)
    }else if(m=='f'){
        document.write(`<h2>${Math.floor(n)}</h2>`)
    }else if(m=='r'){
        document.write(`<h2>${Math.round(n)}</h2>`)
    }else{
        document.write(`<h2>Error!!! In valid mode!!!</h2>`)
    }
}

// practice2_task4(3.43, 'c')
// practice2_task4(3.43, 'f')
// practice2_task4(3.43, 'r')
// practice2_task4(3.43, 'k')

function practice1_task14(m, n){
    if( m > n ){
        for(i = m; i >= n;  i--){
            console.log(i)
            document.write(`${i} <br>`)
        }
    }else{
        for(i = m; i <= n;  i++){
            console.log(i)
            document.write(`${i} <br>`)
        }
    }   
}

practice1_task14(23, 34)
document.write("<hr>")
practice1_task14(34, 20)


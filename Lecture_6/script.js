function q_2_task_1(){
    var k = prompt("k=") 
    var p = prompt("p=")
    var n = prompt("n=")
    // var k=10, p=16, n=7 
    var i = 1
    while(i<=n){
        let r = Math.floor(Math.random()*(p-k+1))+k
        if(r%2==1){
            console.log(r)
            i++
        }
    }
}

// q_2_task_1()

function q_2_task_2(){
    var n = 7
    var anb = "abcdefghijklmnop"
    var word = "a"
    for(let i=0; i<n-1; i++){
        word += anb[Math.floor(Math.random()*anb.length)]
    }
    // console.log(word)
    return word
}

// q_2_task_2()

function q_2_task_2_1(){
    var sentence = ""
    for(let i=0; i<10; i++){
        sentence += q_2_task_2()+" "
    }
    console.log(sentence)
}

// q_2_task_2_1()

function q_2_task_3(id){
    var digits = "0123456789"
    if(id.length != 11){
        return false
    }else{
        for(let i=0; i<id.length; i++){
            if(digits.indexOf(id[i]) == -1 ){
               return false
            }
        }
    }
    return true;
}

console.log(q_2_task_3("00098792781"))  // True

console.log(q_2_task_3("00098792731"))   // False

console.log(q_2_task_3("00098792"))       // False

console.log(q_2_task_3("0hgr0978384"))    // False


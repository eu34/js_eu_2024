function get_random_number(min, max){
    var r = Math.floor(Math.random()*(max-min+1)) + min
    console.log(r)
}
// get_random_number(3, 9)

var ds = document.getElementsByTagName("div")

function dom_1(){   
    console.log(ds)
    // ds[0].innerText = 1
    // ds[1].innerText = 2
    // ds[2].innerText = 3
    // ds[3].innerText = 4
    for(let i=0; i<ds.length; i++){
        ds[i].innerText = i+1
    }
    add_event()
}

// dom_1()

function dom_2(){
    for(let i=0; i < ds.length-1; i+=2){
        ds[i].style.minHeight = "20px"
        ds[i+1].style.minHeight = "20px"
        ds[i].style.background = "yellow"
        ds[i+1].style.background = "pink"
    }
   if(ds.length % 2 == 1){
        console.log(ds.length-1)
        ds[ds.length-1].style.minHeight = "20px"
        ds[ds.length-1].style.background = "yellow"
   }
   add_event()
}

function dom_3(){
    // let n = prompt("Enter number of div: ")
    let n_d = document.createElement("div")
    n_d.innerText = "Added"
    console.log(document.body)
    console.log(ds[ds.length-1].nextSibling)
    document.body.insertBefore(n_d, ds[ds.length-1].nextSibling)
    add_event()
}

function add_event(){
    let ds = document.getElementsByTagName("div")
    for(let i=0; i<ds.length; i++){
        ds[i].addEventListener("click", function(){
            // console.log(this);
            // console.log(this.parentElement)
            this.parentElement.removeChild(this)
        })
    }
}


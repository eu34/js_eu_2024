var students = {
    name: "Giorgi",
    lastname: "Eradze",
    age:19,
    gpa: 4,
    subject: ["Web Technology", "Programming", "Data Base"] 
}

function show_students(){
    var r = document.querySelector("#result").children[0]
    r.innerHTML = students.name+"<br>"
    r.innerHTML += students.lastname+"<br>"
    r.innerHTML += students.age+"<br>"
    r.innerHTML += students.gpa+"<br>"
    r.innerHTML += students.subject+"<br>"
}

var days = ["კვირა", "ორშაბათი", "სამშაბათი", "ოთხშაბათი"]

function test_time_1(){  
    var r = document.querySelector("#result").children[1]
    var d = new Date()
    r.innerHTML = d+"<br>"
    r.innerHTML += days[d.getDay()]+" <hr> "
    r.innerHTML += d.getHours()+":"+d.getMinutes()+":"+d.getSeconds()
}

function test_timing_1(){
    var p = prompt("Grade Subjet Quality: ")
    setTimeout(function(){
        var n_d = document.createElement("div")
        var w = 700 * parseInt(p) / 10
        n_d.classList.add("new")
        n_d.style.width = w+"px"
        var r = document.querySelector("#result")
        console.log(n_d)
        r.appendChild(n_d)
    }, 300)
}

function test_timing_2(){
    var c = 0;
    var s_id = setInterval(function(){
        var p = Math.floor(Math.random()*11)
        var n_d = document.createElement("div")
        var w = 700 * parseInt(p) / 10
        n_d.classList.add("new")
        n_d.style.width = w+"px"
        var r = document.querySelector("#result")
        console.log(c)
        c++
        if(c==10){
            clearInterval(s_id)
        }
        r.appendChild(n_d)
    }, 1000)
}